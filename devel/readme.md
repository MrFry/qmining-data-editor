# Install

`npm install`

# Developments server

`npm run dev`

# Next.js

A projekt Next.js keretrendszerben készült, ami nagyban hasonlít a React.js-hez. Ehhez minden
tudnivaló itt található meg:

https://nextjs.org/learn/basics/getting-started

# Export statikus fájlokba

`npm run export`
