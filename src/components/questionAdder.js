import React, { useState, useEffect } from 'react'

import Question from '../components/Question'

import styles from './questionAdder.module.css'
import constants from '../constants.js'
import commonStyles from '../commonStyles.module.css'

const handleSubmit = async (form) => {
    return new Promise((resolve, reject) => {
        if (!form.subj) {
            reject('nosubj')
            return
        }
        let isValid = form.quiz.every((x) => {
            return x.Q && x.A
        })
        if (!isValid || form.quiz.length === 0) {
            reject('notvalid')
            return
        }

        const t = document.getElementById('cid').value
        let cid = ''
        let version = ''
        if (t) {
            cid = t.split('|')[0]
            version = t.split('|')[1]
        }

        // console.log(form)
        // {
        //   "quiz": [
        //     {
        //       "Q": "aaaaaaaaaaaa",
        //       "A": "bbbbbbbbbbbbbb",
        //       "data": {
        //         "type": "simple"
        //       }
        //     }
        //   ],
        //   "selectedDb": {
        //     "path": "questionDbs/elearning.uni-obuda.hu.json",
        //     "name": "elearning.uni-obuda.hu"
        //   },
        //   "subj": "Elektronika"
        // }

        const toSend = {
            id: cid,
            version: `WEBSITE${version ? ` (${version})` : ''}`,
            location: `https://${form.selectedDb.name}`,
            subj: form.subj,
            quiz: form.quiz,
        }

        fetch(constants.apiUrl + 'isAdding', {
            method: 'POST',
            credentials: 'include',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(toSend),
        })
            .then((res) => {
                return res.json()
            })
            .then((resp) => {
                resolve(resp)
            })
    })
}

const renderUsage = () => {
    return (
        <ul>
            <li>Ezek a kérdések ellenőrizve lesznek hogy megvannak-e már</li>
            <li>
                {
                    "Ha több válasz van, akkor ', '-vel válaszd el őket ( 'válasz1, válasz2, válasz3' )"
                }
            </li>
            <li>
                Kérdéseknél az utolsó sor (ahol a JSON cucc van) jelenleg nem
                módosítható, csak olyan kérdéseket lehet beküldeni, amik sima
                kérdés-válaszok, szóval pl nincs benne kép. Ez később bővül majd
            </li>
            <li>
                Ha sok új kérdést küldesz be, akkor akár több percig is
                eltarthat a dolog. Akárhány kérdést be lehet egyszerre küldeni,
                de max 10-15 az ajánlott
            </li>
            <li>
                Bármilyen szöveget beküldhettek, de ne tegyétek, más
                felhasználóknak és magatoknak lesz rosz, ty!
            </li>
        </ul>
    )
}

const getDefaultQuestion = () => {
    return {
        Q: '',
        A: '',
        data: { type: 'simple' },
    }
}

export default function QuestionAdder({ data, selectedDb, refetchDbs }) {
    const [form, setForm] = useState({ quiz: [getDefaultQuestion()] })
    const [subjects, setSubjects] = useState(null)
    const [isSubmitting, setIsSubmitting] = useState(false)
    const [isNewSubj, setIsNewSubj] = useState(false)

    useEffect(() => {
        if (selectedDb) {
            setForm({
                ...form,
                selectedDb: selectedDb,
            })
        }
    }, [selectedDb])

    useEffect(() => {
        if (data) {
            setSubjects(
                data.map((subj) => {
                    return subj.Name
                })
            )
        }
    }, [data])

    const handleQuestionChange = (index, newVal) => {
        setForm({
            ...form,
            quiz: form.quiz.map((q, i) => {
                if (i !== index) {
                    return q
                } else {
                    return newVal
                }
            }),
        })
    }

    const deleteQuestion = (index) => {
        let quiz = form.quiz

        quiz.splice(index, 1)

        setForm({
            ...form,
            quiz: quiz,
        })
    }

    const renderStuff = () => {
        return (
            <div>
                {form.quiz.map((q, i) => {
                    return (
                        <React.Fragment key={i}>
                            <hr />
                            <Question
                                index={i}
                                question={form.quiz[i] || {}}
                                onChange={(newVal) => {
                                    handleQuestionChange(i, newVal)
                                }}
                            />
                            <div className={commonStyles.actions}>
                                <div
                                    onClick={() => {
                                        deleteQuestion(i)
                                    }}
                                >
                                    Törlés
                                </div>
                            </div>
                        </React.Fragment>
                    )
                })}
                <hr />
                <div className={commonStyles.actions}>
                    <div
                        onClick={() => {
                            let quiz = form.quiz
                            quiz.push(getDefaultQuestion())
                            setForm({
                                ...form,
                                quiz: quiz,
                            })
                        }}
                    >
                        Új kérdés
                    </div>
                    <div
                        className={`${isSubmitting ? styles.issubmitting : ''}`}
                        onClick={() => {
                            if (isSubmitting) {
                                return
                            }
                            setIsSubmitting(true)
                            handleSubmit(form)
                                .then((res) => {
                                    // console.log(res)
                                    // {
                                    //   "success": true,
                                    //   "newQuestions": [
                                    //     {
                                    //       "newQuestions": 1,
                                    //       "qdbName": "elearning.uni-obuda.hu"
                                    //     }
                                    //   ],
                                    //   "totalNewQuestions": 1
                                    // }

                                    if (res.success) {
                                        alert(
                                            `Sikeres beküldés, ${res.totalNewQuestions} új kérdés`
                                        )
                                    } else {
                                        alert('Hiba beküldés közben :/')
                                    }
                                    refetchDbs()
                                    setIsSubmitting(false)
                                })
                                .catch((res) => {
                                    if (res === 'nosubj') {
                                        alert('Nem választottál ki tantárgyat!') // eslint-disable-line
                                    }
                                    if (res === 'notvalid') {
                                        alert('Kérdés kitöltése kötelező!') // eslint-disable-line
                                    }
                                    setIsSubmitting(false)
                                })
                        }}
                    >
                        {isSubmitting
                            ? 'Beküldés folyamatban ...'
                            : 'Kérdések beküldése'}
                    </div>
                </div>
                <input type="text" id="cid" name="cid" hidden />
            </div>
        )
    }

    const renderSubjSelector = () => {
        return (
            <div className={styles.subjSelectorContainer}>
                {isNewSubj ? (
                    <input
                        placeholder="Új tárgy neve..."
                        type="text"
                        className={styles.questionInput}
                        onChange={(event) => {
                            setForm({
                                ...form,
                                subj: event.target.value,
                            })
                        }}
                    />
                ) : (
                    <select
                        onChange={(event) => {
                            setForm({
                                ...form,
                                subj: subjects[event.target.value],
                            })
                        }}
                    >
                        <option key={-1} value={-1}>
                            Válassz egy tárgyat...
                        </option>
                        {subjects.map((subjName, i) => {
                            return (
                                <option key={i} value={i}>
                                    {subjName}
                                </option>
                            )
                        })}
                    </select>
                )}
                <div
                    className={commonStyles.actions}
                    onClick={() => {
                        setIsNewSubj(!isNewSubj)
                    }}
                >
                    <div>{isNewSubj ? 'Létező tárgy ...' : 'Új tárgy ...'}</div>
                </div>
            </div>
        )
    }

    if (!data) {
        return null
    }

    return (
        <div>
            <hr />
            {renderUsage()}
            {subjects ? (
                <>
                    <hr />
                    {renderSubjSelector()}
                    {renderStuff()}
                </>
            ) : null}
        </div>
    )
}
