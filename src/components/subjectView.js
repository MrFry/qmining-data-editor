import React, { useState, useEffect } from 'react'

import LoadingIndicator from '../components/LoadingIndicator.js'
import Subject from '../components/Subject.js'
import SubjectSelector from '../components/SubjectSelector.js'
import SearchBar from '../components/searchBar'

import constants from '../constants.js'
// import styles from './subjectView.module.css'
import commonStyles from '../commonStyles.module.css'

const onSave = (subjName, changedQuestions, deletedQuestions, selectedDb) => {
    return new Promise((resolve) => {
        fetch(constants.apiUrl + 'updateQuestion', {
            method: 'POST',
            credentials: 'include',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                subjName: subjName,
                changedQuestions: changedQuestions,
                deletedQuestions: deletedQuestions,
                type: 'subjEdit',
                selectedDb: selectedDb,
            }),
        })
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                resolve(res)
            })
    })
}

export default function SubjectView(props) {
    const { data, selectedDb } = props
    const [activeSubjName, setActiveSubjName] = useState('')
    const [searchTerm, setSearchTerm] = useState('')

    const [unsavedIndexes, setUnsavedIndexes] = useState([])
    const [editedIndexes, setEditedIndexes] = useState([])
    const [deletedIndexes, setDeletedIndexes] = useState([])
    const [subj, setSubj] = useState(null)

    const hasChange = editedIndexes.length > 0 || deletedIndexes.length > 0

    useEffect(() => {
        let currSubj = data.find((subj) => {
            return subj.Name === activeSubjName
        })
        setSubj(currSubj)
    }, [activeSubjName])

    const resetQuestion = (i) => {
        if (deletedIndexes.includes(i)) {
            setDeletedIndexes(
                deletedIndexes.filter((ind) => {
                    return ind !== i
                })
            )
        }

        if (editedIndexes.includes(i)) {
            setEditedIndexes(
                editedIndexes.filter((ind) => {
                    return ind !== i
                })
            )
        }

        if (unsavedIndexes.includes(i)) {
            setUnsavedIndexes(
                unsavedIndexes.filter((ind) => {
                    return ind !== i
                })
            )
        }

        let currSubj = data.find((subj) => {
            return subj.Name === activeSubjName
        })

        setSubj({
            ...subj,
            Questions: subj.Questions.map((q, j) => {
                if (i === j) {
                    return currSubj.Questions[i]
                } else {
                    return q
                }
            }),
        })
    }

    const handleQuestionChange = (newq, i) => {
        if (!unsavedIndexes.includes(i)) {
            setUnsavedIndexes([...unsavedIndexes, i])
        }

        if (editedIndexes.includes(i)) {
            setEditedIndexes(
                editedIndexes.filter((ind) => {
                    return ind !== i
                })
            )
        }

        setSubj({
            ...subj,
            Questions: subj.Questions.map((q, j) => {
                if (i !== j) {
                    return q
                } else {
                    return newq
                }
            }),
        })
    }

    const saveQuestion = (i) => {
        setUnsavedIndexes(
            unsavedIndexes.filter((ind) => {
                return ind !== i
            })
        )
        setEditedIndexes([...editedIndexes, i])
    }

    const deleteQuestion = (i) => {
        setDeletedIndexes([...deletedIndexes, i])
    }

    const handleSave = () => {
        if (unsavedIndexes.length > 0) {
            alert(
                'Mentetlen módosításaid vannak, kérek mentsd, vagy állítsd vissza azokat'
            )
            return
        }
        if (editedIndexes.length === 0 && deletedIndexes.length === 0) {
            alert('Nem módosítottál még semmit')
            return
        }
        const changedQuestions = editedIndexes.map((ei) => {
            return {
                index: ei,
                value: subj.Questions[ei],
            }
        })
        const deletedQuestions = deletedIndexes
        onSave(subj.Name, changedQuestions, deletedQuestions, selectedDb).then(
            (res) => {
                if (res.success) {
                    alert('Sikeres mentés')
                } else {
                    alert('Hiba mentés közben :/')
                }

                setUnsavedIndexes([])
                setEditedIndexes([])
                setDeletedIndexes([])
            }
        )
    }

    if (data) {
        return (
            <div>
                <SearchBar
                    value={searchTerm}
                    onChange={(e) => setSearchTerm(e)}
                />
                <hr />
                <SubjectSelector
                    data={data}
                    activeSubjName={activeSubjName}
                    searchTerm={searchTerm}
                    onSubjSelect={(subjName) => {
                        if (
                            !hasChange ||
                            confirm(
                                'Mentetlen módosításaid vannak a tárgynál, biztos bezárod?'
                            )
                        ) {
                            setActiveSubjName(subjName)
                            setUnsavedIndexes([])
                            setEditedIndexes([])
                            setDeletedIndexes([])
                        } else {
                            console.log('canceld')
                        }
                    }}
                />
                <hr />
                <div className={commonStyles.actions}>
                    {subj && (
                        <div
                            onClick={() => {
                                handleSave()
                            }}
                        >
                            Tárgy módosításainak mentése
                        </div>
                    )}
                </div>
                <div>
                    {subj && (
                        <Subject
                            subj={subj}
                            unsavedIndexes={unsavedIndexes}
                            editedIndexes={editedIndexes}
                            deletedIndexes={deletedIndexes}
                            resetQuestion={resetQuestion}
                            handleQuestionChange={handleQuestionChange}
                            saveQuestion={saveQuestion}
                            deleteQuestion={deleteQuestion}
                        />
                    )}
                </div>
            </div>
        )
    } else {
        return <LoadingIndicator />
    }
}
