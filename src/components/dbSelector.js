import React from 'react'

export default function DbSelector(props) {
    const { selectedDb, qdbs, onChange, hideLockedDbs } = props
    const selectedIndex =
        qdbs && selectedDb
            ? qdbs.findIndex((qdb) => {
                  return qdb.name === selectedDb.name
              })
            : -1

    return (
        <>
            <select
                style={{ margin: '10px 0px' }}
                defaultValue={selectedIndex}
                value={selectedIndex}
                onChange={(event) => {
                    onChange(qdbs[event.target.value])
                }}
            >
                <option value={-1}>
                    {' -- Válassz egy kérdés adatbázist -- '}
                </option>
                {qdbs.map((qdb, i) => {
                    if (hideLockedDbs && qdb.locked) {
                        return null
                    }

                    return (
                        <option value={i} key={qdb.name}>
                            {qdb.name}
                        </option>
                    )
                })}
            </select>
        </>
    )
}
