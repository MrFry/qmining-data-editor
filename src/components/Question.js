import React from 'react'

import styles from './question.module.css'

const overflowLength = 140

export default function Question({ question, onChange, index }) {
    // FIXME: focus change when input changes to textarea
    const possibleAnswers =
        question.possibleAnswers || question.data.possibleAnswers

    return (
        <div className={styles.questionContainer}>
            {question.Q && question.Q.length > overflowLength ? (
                <textarea
                    placeholder="Kérdés..."
                    value={question.Q}
                    onChange={(e) => {
                        onChange({
                            ...question,
                            Q: e.target.value,
                        })
                    }}
                />
            ) : (
                <input
                    placeholder="Kérdés..."
                    type="text"
                    value={question.Q}
                    onChange={(e) => {
                        onChange({
                            ...question,
                            Q: e.target.value,
                        })
                    }}
                />
            )}
            {question.A && question.A.length > overflowLength ? (
                <textarea
                    placeholder="Helyes válasz..."
                    value={question.A}
                    onChange={(e) => {
                        onChange({
                            ...question,
                            A: e.target.value,
                        })
                    }}
                />
            ) : (
                <input
                    placeholder="Helyes válasz..."
                    type="text"
                    value={question.A}
                    onChange={(e) => {
                        onChange({
                            ...question,
                            A: e.target.value,
                        })
                    }}
                />
            )}
            {possibleAnswers && possibleAnswers.length > 0 ? (
                <>
                    <div className={styles.text}>Lehetséges válaszok:</div>
                    <div className={styles.possibleAnswers}>
                        {possibleAnswers.map((possibleAnswer, i) => {
                            // {"text":"a. Kétismeretlenes egyenletrendszert használunk.",
                            // "selectedByUser":true}

                            console.log(possibleAnswer)
                            const pa =
                                typeof possibleAnswer === 'string'
                                    ? possibleAnswer
                                    : possibleAnswer.val
                                    ? possibleAnswer.val
                                    : possibleAnswer.text

                            return (
                                <div key={i}>
                                    <div>
                                        <input
                                            onChange={() => {
                                                onChange({
                                                    ...question,
                                                    A: pa,
                                                })
                                            }}
                                            checked={pa === question.A}
                                            value={pa}
                                            type="radio"
                                            name={`possiblea${
                                                index ? index : ''
                                            }`}
                                        />
                                        <span>{pa}</span>
                                    </div>
                                    <span
                                        onClick={() => {
                                            const newPossibleAnswers =
                                                possibleAnswers.filter(
                                                    (pa, j) => {
                                                        return j !== i
                                                    }
                                                )
                                            // FIXME: 2 possible answers?
                                            onChange({
                                                ...question,
                                                data: {
                                                    ...question.data,
                                                    possibleAnswers:
                                                        newPossibleAnswers,
                                                },
                                                possibleAnswers:
                                                    newPossibleAnswers,
                                            })
                                        }}
                                        className={styles.delete}
                                    >
                                        Törlés
                                    </span>
                                </div>
                            )
                        })}
                    </div>
                </>
            ) : null}
            <input
                type="text"
                value={JSON.stringify(question.data)}
                name="data"
                onChange={(e) => {
                    console.log(e.target.value)
                }}
            />
        </div>
    )
}
