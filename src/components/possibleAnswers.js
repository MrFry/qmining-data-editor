import React, { useEffect, useState } from 'react'

import LoadingIndicator from '../components/LoadingIndicator'
import SearchBar from '../components/searchBar'
import TestView from '../components/testView'

import constants from '../constants.js'
import styles from './possibleAnswers.module.css'

const Infos = () => {
    return (
        <div className={styles.infoContainer}>
            <div>
                Itt azok a tesztek találhatók, amiknek a kitöltése után nem volt
                ellenőrző oldal. A script így nem tudja, hogy melyik a helyes
                megoldás. De ha ti igen, akkor jelöljétek be / írjátok be, és
                mentsétek el. Így mikor legközelebb találkoztok a kérdéssel a
                script tudni fogja a helyes választ. Ezzel másoknak is nagyon
                sokat segítetek.
            </div>
            <div>
                Jelenleg azok a tesztek is megjelennek itt, amiknek elérhető a
                teszt eredmény oldala, és meg is van hozzá a helyes válasz. A
                jövőben ezek ki lesznek automatikusan törölve.
            </div>
        </div>
    )
}

const fetchPossibleAnswers = () => {
    return new Promise((resolve) => {
        fetch(`${constants.apiUrl}possibleAnswers`, {
            credentials: 'include',
        })
            .then((resp) => {
                return resp.json()
            })
            .then((res) => {
                resolve(res)
            })
    })
}

const fetchSubject = (subj, savedQuestionsFileName) => {
    return new Promise((resolve) => {
        fetch(
            `${constants.apiUrl}savedQuestions/${subj}/${savedQuestionsFileName}`,
            {
                credentials: 'include',
            }
        )
            .then((resp) => {
                return resp.json()
            })
            .then((resp) => {
                resolve(resp)
            })
    })
}

const fetchTest = (subj, test) => {
    return new Promise((resolve) => {
        fetch(`${constants.apiUrl}savedQuestions/${subj}/${test}`, {
            credentials: 'include',
        })
            .then((resp) => {
                return resp.json()
            })
            .then((resp) => {
                resolve(resp)
            })
    })
}

export default function PossibleAnswers({ router, refetchDbs }) {
    const [currSubjName, setCurrSubjName] = useState(null)
    const [currTestName, setCurrTestName] = useState(null)

    const [subjects, setSubjects] = useState([])
    const [currSubj, setCurrSubj] = useState(null)
    const [currTest, setCurrTest] = useState(null)

    const [savedQuestionsFileName, setSavedQuestionsFileName] = useState(null)
    const [searchTerm, setSearchTerm] = useState('')

    useEffect(() => {
        fetchPossibleAnswers().then((resp) => {
            setSubjects(resp.subjects)
            setSavedQuestionsFileName(resp.savedQuestionsFileName)

            const subj = router.query.subj
                ? decodeURIComponent(router.query.subj)
                : ''
            const test = router.query.test
                ? decodeURIComponent(router.query.test)
                : ''

            if (subj) {
                fetchSubject(subj, resp.savedQuestionsFileName).then((resp) => {
                    setCurrSubj(resp)
                    setCurrSubjName(subj)
                    router.push(
                        `${router.pathname}?v=pa&subj=${encodeURIComponent(
                            subj
                        )}`,
                        undefined,
                        { shallow: true }
                    )
                    if (subj && test) {
                        fetchTest(subj, test).then((resp) => {
                            setCurrTest(resp)
                            setCurrTestName(test)
                            router.push(
                                `${
                                    router.pathname
                                }?v=pa&subj=${encodeURIComponent(
                                    subj
                                )}&test=${encodeURIComponent(test)}`,
                                undefined,
                                { shallow: true }
                            )
                        })
                    }
                })
            }
        })
    }, [])

    const renderStuff = () => {
        if (subjects && currSubj && currTest) {
            return (
                <>
                    <div
                        className={styles.backButton}
                        onClick={() => {
                            setCurrTest(null)
                            router.back()
                        }}
                    >
                        Vissza
                    </div>
                    <div>
                        {currTest && (
                            <TestView
                                subjName={currSubjName}
                                testName={currTestName}
                                test={currTest}
                                router={router}
                                onDelete={() => {
                                    refetchDbs()
                                    setCurrTest(null)
                                    fetchSubject(
                                        currSubjName,
                                        savedQuestionsFileName
                                    ).then((resp) => {
                                        setCurrSubj(resp)
                                    })
                                }}
                            />
                        )}
                    </div>
                </>
            )
        } else if (subjects && currSubj) {
            return (
                <>
                    <div className={styles.headerContainer}>
                        <div
                            className={styles.backButton}
                            onClick={() => {
                                setCurrSubj(null)
                                router.back()
                            }}
                        >
                            Vissza
                        </div>
                        <div className={styles.subjName}>{currSubjName}</div>
                    </div>
                    <div className={styles.tableContainer}>
                        <>
                            <div>
                                <div>Dátum</div>
                                <div>Felhasználó ID</div>
                                <div>Tárgy</div>
                                <div>Teszt URL</div>
                            </div>
                            {currSubj &&
                                currSubj.map((test, i) => {
                                    return (
                                        <div
                                            className={styles.testContainer}
                                            key={i}
                                            onClick={() => {
                                                setCurrTestName(test.fname)
                                                router.push(
                                                    `${
                                                        router.pathname
                                                    }?v=pa&subj=${encodeURIComponent(
                                                        currSubjName
                                                    )}&test=${encodeURIComponent(
                                                        test.fname
                                                    )}`,
                                                    undefined,
                                                    { shallow: true }
                                                )

                                                fetchTest(
                                                    currSubjName,
                                                    test.fname
                                                ).then((resp) => {
                                                    setCurrTest(resp)
                                                })
                                            }}
                                        >
                                            <div>
                                                {new Date(
                                                    test.date
                                                ).toLocaleString()}
                                            </div>
                                            <div>{test.userid}</div>
                                            <div>{test.subj}</div>
                                            <div>{test.testUrl}</div>
                                        </div>
                                    )
                                })}
                        </>
                    </div>
                </>
            )
        } else if (subjects) {
            return (
                <>
                    <Infos />
                    <SearchBar
                        value={searchTerm}
                        onChange={(e) => setSearchTerm(e)}
                    />
                    <div className={styles.tableContainer}>
                        <>
                            <div>Tárgy neve</div>
                            {subjects.map((subj, i) => {
                                if (
                                    !subj.name
                                        .toLowerCase()
                                        .includes(searchTerm.toLowerCase())
                                ) {
                                    return null
                                }
                                return (
                                    <div
                                        key={i}
                                        onClick={() => {
                                            setCurrSubjName(subj.name)
                                            router.push(
                                                `${
                                                    router.pathname
                                                }?v=pa&subj=${encodeURIComponent(
                                                    subj.name
                                                )}`,
                                                undefined,
                                                { shallow: true }
                                            )

                                            fetchSubject(
                                                subj.name,
                                                savedQuestionsFileName
                                            ).then((resp) => {
                                                setCurrSubj(resp)
                                            })
                                        }}
                                    >
                                        {subj.name}
                                    </div>
                                )
                            })}
                        </>
                    </div>
                </>
            )
        } else {
            return <LoadingIndicator />
        }
    }

    return renderStuff()
}
