import React, { PureComponent } from 'react'

import Question from './Question.js'

import styles from './Questions.module.css'
import commonStyles from '../commonStyles.module.css'

class Questions extends PureComponent {
    render() {
        const { subjs, onChange } = this.props

        return (
            <div>
                {subjs.map((subj) => {
                    return (
                        <div key={subj.Name}>
                            <div className={styles.subjName}>{subj.Name}</div>
                            {subj.Questions.map((qo, i) => {
                                const question = qo.q
                                const unsaved = qo.unsaved

                                return (
                                    <React.Fragment key={i}>
                                        <hr />
                                        <div
                                            className={`${
                                                unsaved ? styles.unsaved : ''
                                            }`}
                                        >
                                            <Question
                                                index={`${subj.Name}_${i}`}
                                                question={question}
                                                onChange={(newVal) => {
                                                    onChange({
                                                        index: i,
                                                        subjName: subj.Name,
                                                        type: 'edit',
                                                        newVal: newVal,
                                                    })
                                                }}
                                            />
                                            <div
                                                className={commonStyles.actions}
                                            >
                                                <div
                                                    onClick={() => {
                                                        onChange({
                                                            index: i,
                                                            subjName: subj.Name,
                                                            type: 'reset',
                                                        })
                                                    }}
                                                >
                                                    Visszaállítás
                                                </div>
                                                <div
                                                    onClick={() => {
                                                        onChange({
                                                            index: i,
                                                            subjName: subj.Name,
                                                            type: 'save',
                                                        })
                                                    }}
                                                >
                                                    Mentés
                                                </div>
                                                <div
                                                    onClick={() => {
                                                        onChange({
                                                            index: i,
                                                            subjName: subj.Name,
                                                            type: 'delete',
                                                        })
                                                    }}
                                                >
                                                    Törlés
                                                </div>
                                            </div>
                                        </div>
                                    </React.Fragment>
                                )
                            })}
                        </div>
                    )
                })}
            </div>
        )
    }
}

export default Questions
