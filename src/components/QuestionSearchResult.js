import React from 'react'

import Questions from './Questions.js'

import constants from '../constants.js'

const countReducer = (acc, subj) => {
    return acc + subj.Questions.length
}

export default function QuestionSearchResult(props) {
    const { data, searchTerm, edits, onChange } = props

    let subjs = []
    let results = -1

    if (searchTerm || edits.length > 0) {
        subjs = data.reduce((acc, subj) => {
            const resultQuestions = subj.Questions.reduce(
                (qacc, question, i) => {
                    const unsaved = edits.some((e) => {
                        if (
                            e.subjName === subj.Name &&
                            e.index === i &&
                            e.type === 'edit'
                        ) {
                            return true
                        }
                    })
                    if (unsaved) {
                        qacc.push({
                            q: question,
                            unsaved: unsaved,
                        })
                        return qacc
                    }

                    const keys = ['Q', 'A', 'data']
                    keys.some((key) => {
                        if (typeof question[key] !== 'string') {
                            return false
                        }
                        if (
                            searchTerm &&
                            question[key] &&
                            question[key]
                                .toLowerCase()
                                .includes(searchTerm.toLowerCase())
                        ) {
                            qacc.push({ q: question })
                            return true
                        }
                    })
                    return qacc
                },
                []
            )
            if (resultQuestions.length > 0) {
                acc.push({
                    Name: subj.Name,
                    Questions: resultQuestions,
                    ind: subj.ind,
                })
            }
            return acc
        }, [])
        results = subjs.reduce(countReducer, 0)
    } else {
        results = data.reduce(countReducer, 0)
    }

    console.log(subjs)

    const renderCount = () => {
        return (
            <div>
                {searchTerm ? '' : 'Kezdj el írni kereséshez!'} {results}{' '}
                {searchTerm ? 'találat' : 'kérdés'}{' '}
                {searchTerm ? subjs.length : data.length} tárgy
            </div>
        )
    }

    if (results > constants.maxQuestionsToRender) {
        return renderCount()
    } else {
        return (
            <div>
                <div>{renderCount()}</div>
                <div>
                    <Questions subjs={subjs} onChange={onChange} />
                </div>
            </div>
        )
    }
}
