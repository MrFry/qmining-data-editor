import React, { useState, useEffect } from 'react'

import LoadingIndicator from '../components/LoadingIndicator.js'
import QuestionSearchResult from '../components/QuestionSearchResult.js'
import SearchBar from '../components/searchBar'

import constants from '../constants.js'
// import styles from './questionView.module.css'

const updateQuestion = (e, selectedDb) => {
    return new Promise((resolve) => {
        fetch(constants.apiUrl + 'updateQuestion', {
            method: 'POST',
            credentials: 'include',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                ...e,
                selectedDb: selectedDb,
            }),
        })
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                resolve(res)
            })
    })
}

const rmQuestion = (e, selectedDb) => {
    return new Promise((resolve) => {
        fetch(constants.apiUrl + 'updateQuestion', {
            method: 'POST',
            credentials: 'include',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                ...e,
                selectedDb: selectedDb,
            }),
        })
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                resolve(res)
            })
    })
}

export default function questionView(props) {
    const { selectedDb } = props
    const [data, setData] = useState(props.data)
    const [searchTerm, setSearchTerm] = useState('')
    const [edits, setEdits] = useState([])

    useEffect(() => {
        setData(props.data)
    }, [props.data])

    const updateEdits = (e) => {
        setEdits(
            edits.map((edit) => {
                if (edit.index === e.index) {
                    return e
                } else {
                    return edit
                }
            })
        )
    }

    const removeFromEdits = (e) => {
        setEdits(
            edits.filter((edit, i) => {
                return i !== e.index
            })
        )
    }

    const onChange = (e) => {
        const editIndex = edits.findIndex((edit) => {
            return edit.subjName === e.subjName && edit.index === e.index
        })
        if (editIndex === -1) {
            if (e.type === 'edit') {
                if (editIndex === -1) {
                    setEdits([...edits, e])
                }
            }
            if (e.type === 'delete') {
                rmQuestion(e, selectedDb).then((res) => {
                    if (res.success) {
                        alert('Sikeres törlés')
                    } else {
                        alert('Hiba mentés közben :/')
                    }

                    setData(
                        data.map((subj) => {
                            if (subj.Name !== e.subjName) {
                                return subj
                            } else {
                                return {
                                    ...subj,
                                    Questions: subj.Questions.filter(
                                        (question, i) => {
                                            return i !== e.index
                                        }
                                    ),
                                }
                            }
                        })
                    )
                })
            }
        } else {
            if (e.type === 'reset') {
                // edits -> saves -> resets? => should do nothing, no reset after saving
                removeFromEdits(e)
                setData(
                    data.map((subj) => {
                        if (subj.Name !== e.subjName) {
                            return subj
                        } else {
                            return {
                                ...subj,
                                Questions: subj.Questions.map((question, i) => {
                                    if (i !== e.index) {
                                        return question
                                    } else {
                                        const ps = props.data.find((subj) => {
                                            return subj.Name === e.subjName
                                        })
                                        if (ps) {
                                            return ps.Questions[i]
                                        }
                                    }
                                }),
                            }
                        }
                    })
                )
            }
            if (e.type === 'save') {
                updateQuestion(
                    edits.find((edit) => {
                        return edit.index === e.index
                    }),
                    selectedDb
                ).then((res) => {
                    if (res.success) {
                        alert('Sikeres mentés')
                    } else {
                        alert('Hiba mentés közben :/')
                    }

                    removeFromEdits(e)
                })
            }
            if (e.type === 'edit') {
                updateEdits(e)
            }
        }

        if (e.type === 'edit') {
            setData(
                data.map((subj) => {
                    if (subj.Name !== e.subjName) {
                        return subj
                    } else {
                        return {
                            ...subj,
                            Questions: subj.Questions.map((question, i) => {
                                if (i !== e.index) {
                                    return question
                                } else {
                                    return e.newVal
                                }
                            }),
                        }
                    }
                })
            )
        }
    }

    if (data) {
        return (
            <div>
                <SearchBar
                    value={searchTerm}
                    onChange={(e) => setSearchTerm(e)}
                />
                <hr />
                <div>
                    <QuestionSearchResult
                        onChange={onChange}
                        data={data}
                        searchTerm={searchTerm}
                        edits={edits}
                    />
                </div>
            </div>
        )
    } else {
        return <LoadingIndicator />
    }
}
