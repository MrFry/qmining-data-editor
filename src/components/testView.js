import React, { useState } from 'react'

import Question from '../components/Question'

import constants from '../constants.js'
import styles from './testView.module.css'
import commonStyles from '../commonStyles.module.css'

const rmTest = (subjName, testName) => {
    return new Promise((resolve) => {
        fetch(constants.apiUrl + 'rmPossibleAnswer', {
            method: 'POST',
            credentials: 'include',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                subj: subjName,
                file: testName,
            }),
        })
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                resolve(res)
            })
    })
}

export default function TestView(props) {
    const { subjName, testName, router, onDelete } = props
    const [test, setTest] = useState(props.test)

    const renderActions = () => {
        return (
            <div className={styles.actions}>
                <div
                    onClick={() => {
                        if (
                            confirm(
                                'Biztos véglegesen törlöd ezt az egész tesztet?'
                            )
                        ) {
                            rmTest(subjName, testName).then((res) => {
                                if (res.res === 'ok') {
                                    // alert('sikeres törlés')
                                    router.back()
                                    onDelete()
                                } else {
                                    alert('hiba törlés közben!')
                                }
                            })
                        }
                    }}
                >
                    Teszt törlése
                </div>
                <div
                    onClick={() => {
                        if (
                            confirm(
                                'Biztos beküldöd? Beküldés után törlődik a teszt, de a Kérdések/Tárgyak nézetnél megtalálhatóak lesznek'
                            )
                        ) {
                            const questions = test.questions.map((q) => {
                                return {
                                    Q: q.Q,
                                    A: q.A,
                                    data: {
                                        ...q.data,
                                        ...(q.possibleAnswers && {
                                            possibleAnswers: q.possibleAnswers,
                                        }),
                                    },
                                }
                            })
                            const toSend = {
                                id: 'WEBSITE',
                                version: 'WEBSITE',
                                location: `https://${test.testUrl}`,
                                subj: test.subj,
                                quiz: questions,
                            }

                            fetch(constants.apiUrl + 'isAdding', {
                                method: 'POST',
                                credentials: 'include',
                                headers: {
                                    Accept: 'application/json',
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(toSend),
                            })
                                .then((res) => {
                                    return res.json()
                                })
                                .then((res) => {
                                    if (res.success) {
                                        alert(
                                            `Sikeres beküldés, ${res.totalNewQuestions} új kérdés`
                                        )
                                        rmTest(subjName, testName).then(
                                            (res) => {
                                                if (res.res === 'ok') {
                                                    router.back()
                                                    onDelete()
                                                }
                                            }
                                        )
                                    } else {
                                        alert('Hiba beküldés közben :/')
                                    }
                                })
                        }
                    }}
                >
                    Teszt mentése
                </div>
            </div>
        )
    }

    return (
        <div>
            <div className={styles.headerContainer}>
                <div className={styles.header}>
                    <div>Tárgy neve</div>
                    <div>{test.subj}</div>
                </div>
                <div className={styles.header}>
                    <div>Teszt URL</div>
                    <div>{test.testUrl}</div>
                </div>
                <div className={styles.header}>
                    <div>Beküldő felhasználó ID</div>
                    <div>{test.userid}</div>{' '}
                </div>
                <div className={styles.header}>
                    <div>Beküldés ideje</div>
                    <div>{new Date(test.date).toLocaleString()}</div>{' '}
                </div>
            </div>
            <hr />
            {renderActions()}
            <div className={styles.questionsContainer}>
                {test.questions.map((question, i) => {
                    return (
                        <React.Fragment key={i}>
                            <hr key={`${i}hr`} />
                            <div key={i}>
                                <Question
                                    index={i}
                                    onChange={(newQ) => {
                                        setTest({
                                            ...test,
                                            questions: test.questions.map(
                                                (q, j) => {
                                                    if (j === i) {
                                                        return newQ
                                                    }
                                                    return q
                                                }
                                            ),
                                        })
                                    }}
                                    question={question}
                                />
                                <div className={commonStyles.actions}>
                                    <div
                                        onClick={() => {
                                            setTest({
                                                ...test,
                                                questions:
                                                    test.questions.filter(
                                                        (q, j) => {
                                                            return j !== i
                                                        }
                                                    ),
                                            })
                                        }}
                                    >
                                        Kérdés törlése
                                    </div>
                                </div>
                            </div>
                        </React.Fragment>
                    )
                })}
            </div>
            {test.questions.length > 2 ? (
                <>
                    <hr />
                    {renderActions()}
                </>
            ) : null}
        </div>
    )
}
