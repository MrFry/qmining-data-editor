import React from 'react'

import styles from './searchBar.module.css'

export default function SearchBar(props) {
    const { onChange, value } = props
    return (
        <div className={styles.searchContainer}>
            <input
                placeholder="Keresés..."
                className={styles.searchBar}
                type="text"
                value={value}
                onChange={(e) => {
                    onChange(e.target.value)
                }}
            />
            <button
                onClick={() => {
                    onChange('')
                }}
                className={styles.clearButton}
            >
                ❌
            </button>
        </div>
    )
}
