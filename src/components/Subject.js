import React from 'react'

import Question from './Question.js'

import styles from './subject.module.css'
import commonStyles from '../commonStyles.module.css'

function DeletedQuestion({ reset }) {
    return (
        <div>
            <div className={styles.deletedQuestion}>Törölt kérdés</div>
            <div className={commonStyles.actions}>
                <div
                    onClick={() => {
                        reset()
                    }}
                >
                    Visszaállítás
                </div>
            </div>
        </div>
    )
}

export default function Subject(props) {
    const {
        subj,
        unsavedIndexes,
        deletedIndexes,
        editedIndexes,
        resetQuestion,
        handleQuestionChange,
        saveQuestion,
        deleteQuestion,
    } = props

    if (subj) {
        return (
            <div className={styles.questionContainer}>
                {subj.Questions.map((question, i) => {
                    // FIXME: list edited questions first?
                    const unsaved = unsavedIndexes.includes(i)
                    const edited = editedIndexes.includes(i)
                    const deleted = deletedIndexes.includes(i)
                    return (
                        <React.Fragment key={i}>
                            <hr />
                            {deleted ? (
                                <DeletedQuestion
                                    reset={() => {
                                        resetQuestion(i)
                                    }}
                                    index={i}
                                />
                            ) : (
                                <div
                                    className={`${
                                        unsaved ? styles.unsaved : ''
                                    } ${edited ? styles.edited : ''}`}
                                >
                                    <Question
                                        index={i}
                                        onChange={(newq) => {
                                            handleQuestionChange(newq, i)
                                        }}
                                        question={question}
                                    />
                                    <div className={commonStyles.actions}>
                                        <div
                                            onClick={() => {
                                                resetQuestion(i)
                                            }}
                                        >
                                            Visszaállítás
                                        </div>
                                        <div
                                            onClick={() => {
                                                saveQuestion(i)
                                            }}
                                        >
                                            {edited
                                                ? 'Kérdés mentve'
                                                : 'Kérdés mentése'}
                                        </div>
                                        <div
                                            onClick={() => {
                                                deleteQuestion(i)
                                            }}
                                        >
                                            Kérdés törlése
                                        </div>
                                    </div>
                                </div>
                            )}
                        </React.Fragment>
                    )
                })}
            </div>
        )
    } else {
        return <div />
    }
}
