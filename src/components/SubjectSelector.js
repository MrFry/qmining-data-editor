import React from 'react'

import styles from './SubjectSelector.module.css'

export default function SubjectSelector(props) {
    const { activeSubjName, searchTerm, data, onSubjSelect } = props

    return (
        <div className={styles.subjectSelector}>
            {data.map((subj, i) => {
                if (
                    !subj.Name.toLowerCase().includes(searchTerm.toLowerCase())
                ) {
                    return null
                }

                return (
                    <div
                        className={
                            activeSubjName === subj.Name
                                ? 'subjItem activeSubjItem'
                                : 'subjItem'
                        }
                        key={i}
                        onClick={() => onSubjSelect(subj.Name)}
                    >
                        <span className={styles.subjName}>{subj.Name}</span>
                        <span className={styles.questionCount}>
                            [ {subj.Questions.length} ]
                        </span>
                    </div>
                )
            })}
        </div>
    )
}
