// eslint-disable-next-line no-undef
const useLocalhost = process && process.env.NODE_ENV === 'development'

const constants = {
    siteUrl: useLocalhost ? 'http://localhost:8080/' : '/',
    apiUrl: useLocalhost ? 'http://localhost:8080/api/' : '/api/',
    maxQuestionsToRender: 250,
}

export default constants
