import React, { useState, useEffect } from 'react'
import Head from 'next/head'

import SubjectView from '../components/subjectView'
import QuestionView from '../components/questionView'
import QuestionAdder from '../components/questionAdder.js'
import PossibleAnswers from '../components/possibleAnswers.js'

import DbSelector from '../components/dbSelector.js'
import LoadingIndicator from '../components/LoadingIndicator'

import styles from './index.module.css'
import commonStyles from '../commonStyles.module.css'
import constants from '../constants.js'

const domain = typeof window !== 'undefined' ? window.location.domain : ''

const Infos = ({ onClick, renderOKButton }) => {
    return (
        <div className={commonStyles.infoContainer}>
            <div className={commonStyles.infoHeader}>Kérdés szerkesztő</div>
            <div>
                Ezen az oldalon az éles adatbázisban levő kérdéseket tudod
                szerkeszteni, vagy azokhoz tudsz adni.{' '}
                <b>
                    A törléshez és módosításokhoz nem kér megerősítést, ezek
                    azonnal megtörténnek, és nem visszavonhatóak.
                </b>
            </div>
            <div>
                <i>Néhány dolog, amit kérlek tarts be szerkesztés közben:</i>
                <div style={{ textAlign: 'left', width: '700px' }}>
                    <ul>
                        <li>
                            Ne rontsd el a kérdéseket sok törléssel / rossz
                            válasz megadásával. Sok más felhasználónak lesz
                            rossz, és visszakereshető / tiltható a módosító
                        </li>
                        <li>
                            Arra is vigyázz, hogy véletlen se történjen ilyesmi,
                            vagy ha mégis valami baj történt, akkor azt{' '}
                            <a href={`${constants.siteUrl}irc`}>jelezd</a>. Van
                            sok biztonsági mentés
                        </li>
                        <li>
                            Ahhoz, hogy a script megtalálja a helyes választ a
                            kérdés szövegének <b>pontosan</b> olyannak kell
                            lennie, mint a teszt közben (elírásokkal, ....
                            -okkal, meg mindennel)
                        </li>
                    </ul>
                </div>
                <div>
                    Ha akármi kérdés van{' '}
                    <a
                        href={`${constants.siteUrl}contact`}
                        target={'_blank'}
                        rel={'noreferrer'}
                    >
                        itt lehet üzenetet küldeni
                    </a>
                    .
                </div>
            </div>
            {renderOKButton && (
                <div className={commonStyles.infoReadButton} onClick={onClick}>
                    OK
                </div>
            )}
        </div>
    )
}

const fetchDbs = () => {
    return new Promise((resolve) => {
        fetch(`${constants.apiUrl}getDbs`, {
            credentials: 'include',
        })
            .then((resp) => {
                return resp.json()
            })
            .then((resp) => {
                resolve(resp)
            })
    })
}

const fetchData = (selectedDb) => {
    return new Promise((resolve) => {
        const toFetch = `${constants.apiUrl}${selectedDb.path}`
        fetch(toFetch, {
            credentials: 'include',
        })
            .then((resp) => {
                return resp.json()
            })
            .then((resp) => {
                resolve(resp)
            })
    })
}

const views = {
    welcome: 'w',
    subject: 's',
    question: 'q',
    questionAdder: 'qa',
    possibleAnswers: 'pa',
}

export default function Index({ router }) {
    const [infoRead, setInfoRead] = useState(false)
    const [data, setData] = useState(null)
    const [qdbs, setQdbs] = useState(null)
    const [selectedDb, setSelectedDb] = useState(null)
    const [view, setView] = useState(views.welcome)

    const [error, setError] = useState(null)

    useEffect(() => {
        if (selectedDb) {
            loadData()
        } else {
            setData(null)
        }
    }, [selectedDb])

    useEffect(() => {
        fetchDbs().then((resp) => {
            setQdbs(resp)

            const view = router.query.v
                ? decodeURIComponent(router.query.v)
                : views.welcome
            setView(view)
        })
    }, [])

    const loadData = () => {
        setData(null)
        if (!selectedDb) {
            alert('Válassz egy adatbázist!')
            return
        }

        fetchData(selectedDb)
            .then((resp) => {
                setData(resp)
            })
            .catch((error) => {
                console.error(error)
                console.error('Error while fetching data')
                setError('Error while fetching data')
            })
    }

    const refetchDbs = () => {
        if (selectedDb) {
            fetchData(selectedDb).then((resp) => {
                setData(resp)
            })
        }
        fetchDbs().then((resp) => {
            setQdbs(resp)
        })
    }

    const renderView = () => {
        if (view === views.subject) {
            return (
                <>
                    <Head>
                        <title>Tárgyak - Data Editor | {domain}</title>
                    </Head>
                    <DbSelector
                        qdbs={qdbs}
                        selectedDb={selectedDb}
                        onChange={setSelectedDb}
                    />
                    {data && (
                        <SubjectView selectedDb={selectedDb} data={data} />
                    )}
                </>
            )
        } else if (view === views.question) {
            return (
                <>
                    <Head>
                        <title>Kérdések - Data Editor | {domain}</title>
                    </Head>
                    <DbSelector
                        qdbs={qdbs}
                        selectedDb={selectedDb}
                        onChange={setSelectedDb}
                    />
                    {data && (
                        <QuestionView selectedDb={selectedDb} data={data} />
                    )}
                </>
            )
        } else if (view === views.questionAdder) {
            return (
                <>
                    <Head>
                        <title>Kérdés beküldés - Data Editor | {domain}</title>
                    </Head>
                    <DbSelector
                        hideLockedDbs
                        qdbs={qdbs}
                        selectedDb={selectedDb}
                        onChange={setSelectedDb}
                    />
                    <QuestionAdder
                        data={data}
                        selectedDb={selectedDb}
                        refetchDbs={refetchDbs}
                    />
                </>
            )
        } else if (view === views.possibleAnswers) {
            return (
                <>
                    <Head>
                        <title>
                            Kitöltetlen tesztek - Data Editor | {domain}
                        </title>
                    </Head>
                    <PossibleAnswers refetchDbs={refetchDbs} router={router} />
                </>
            )
        } else if (view === views.welcome) {
            return <Infos renderOKButton={false} />
        } else {
            return <div>No view!</div>
        }
    }

    function renderViewSelector() {
        return (
            <div className={styles.viewButtonContainer}>
                <div
                    className={
                        view === views.question ? styles.activeView : undefined
                    }
                    onClick={() => {
                        router.replace(
                            `${router.pathname}?v=${views.question}`,
                            undefined,
                            { shallow: true }
                        )
                        setView(views.question)
                    }}
                >
                    Kérdések
                </div>
                <div
                    title={
                        'Választott adatbázisban lévő tárgyak megjelenítése, és tárgyakon belüli kérdések szerkesztése'
                    }
                    className={
                        view === views.subject ? styles.activeView : undefined
                    }
                    onClick={() => {
                        router.replace(
                            `${router.pathname}?v=${views.subject}`,
                            undefined,
                            {
                                shallow: true,
                            }
                        )
                        setView(views.subject)
                    }}
                >
                    Tárgyak
                </div>
                <div
                    className={
                        view === views.questionAdder
                            ? styles.activeView
                            : undefined
                    }
                    onClick={() => {
                        router.replace(
                            `${router.pathname}?v=${views.questionAdder}`,
                            undefined,
                            { shallow: true }
                        )
                        setView(views.questionAdder)
                        setSelectedDb(null)
                    }}
                >
                    Kérdés beküldés
                </div>
                <div
                    className={
                        view === views.possibleAnswers
                            ? styles.activeView
                            : undefined
                    }
                    onClick={() => {
                        router.replace(
                            `${router.pathname}?v=${views.possibleAnswers}`,
                            undefined,
                            { shallow: true }
                        )
                        setView(views.possibleAnswers)
                    }}
                >
                    Kitöltetlen tesztek
                </div>
            </div>
        )
    }

    if (error) {
        return <div>{error}</div>
    }

    return <div>A dataeditor jelenleg nem működik!</div>

    if (!infoRead) {
        return <Infos onClick={() => setInfoRead(true)} renderOKButton />
    }

    if (!qdbs) {
        return <LoadingIndicator />
    }

    return (
        <div>
            {renderViewSelector()}
            {renderView()}
        </div>
    )
}
